#!/bin/bash

# Slurm directives for job properties
#SBATCH -J test-gent4-apptainer         # Job name
#SBATCH -q debug                        # Queue (regular, debug, etc.)
#SBATCH -N 1                            # Number of nodes
#SBATCH -C cpu                          # CPU architecture
#SBATCH -t 00:29:00                     # Wall clock time
#SBATCH -e %x-%j.err                    # Error file
#SBATCH -o %x-%j.out                    # Output file
#SBATCH --time-min=00:29:00             # Minimum time allocation
#SBATCH --comment=00:50:00              # Job comment with expected runtime
#SBATCH --signal=SIGTERM@60             # Signal for checkpointing 60 seconds before job ends
#SBATCH --requeue                       # Automatically requeue the job if it terminates
#SBATCH --open-mode=append              # Append output to log files

# Load required module
#SBATCH --module=cvmfs                  # Load CVMFS module for Apptainer

# Set up environment and DMTCP coordinator
export DMTCP_COORD_HOST=$(hostname)
export PATH=${PATH}:/cvmfs/oasis.opensciencegrid.org/mis/apptainer/1.3.3/x86_64/bin

# Requeue function to resubmit the job on SIGTERM
function requeue () {
    echo "Got Signal. Going to requeue"
    scontrol requeue ${SLURM_JOB_ID}
}

# Trap SIGTERM signal to trigger requeue function
trap requeue SIGTERM

# Define the path to the Apptainer image
# download image with 
# apptainer pull docker://mtimalsina/geant4_dmtcp:Dec2023 
apptainer_image_path="/global/cfs/cdirs/m0000/elvis/geant4_dmtcp_Dec2023.sif"

# Check if the Apptainer image exists
if [ ! -f "$apptainer_image_path" ]; then
    echo "Cannot find Apptainer image at $apptainer_image_path"
    exit 1
fi

# Launch the job within the Apptainer container
apptainer exec -B /cvmfs:/cvmfs \
    $apptainer_image_path /bin/bash ./wrapper.sh &

wait