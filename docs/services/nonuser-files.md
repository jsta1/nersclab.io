# Files from Non-Users

Receiving a file from a collaborator who is not a NERSC user can occasionally present a 
challenge, though new tools have arisen to address this issue in recent years. Following 
are some approaches that may be helpful in coordinating a data transfer, based on how 
large the files are. 

## For smallish files (<100s of GBs), your collaborator can

- Send as an email attachment
- Transfer via a common server for which they and you have logins using scp, sftp, or 
rsync
- Share via a cloud storage provider that you both use, such as Box, DropBox, Google 
Drive, Apple iCloud, or Microsoft OneDrive
- Share via file transfer service such as WeTransfer or DropSend
- Post world-readable on a web server somewhere and send you the URL
- Send you a physical USB thumb drive or CD/DVD
- Sign up for a NERSC account and get added to your project
- Use a file sharing service at their or your home institution

## For largish files (100s of GBs or more), your collaborator can

- Transfer via a common server for which they and you have logins, using bbcp or gridftp
- Share via a cloud storage provider that you both use such as Amazon S3 bucket, Google 
Cloud Platform, or Microsoft Azure Blob
- Share via paid-plan file transfer service such as WeTransfer, FileMail, or MyAirBridge
- Post in a world-readable Globus collection and send you the collection identifier
- Sign up for a NERSC account and get added to your project
- Use a file sharing service at their or your home institution

!!! note
    Except for the option of obtaining a NERSC account, NERSC does not provide 
    user support for any of the methods listed above. Commercial services are 
    listed only as examples; we do not endorse any particular provider.
