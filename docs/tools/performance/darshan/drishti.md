# Drishti

[Drishti I/O](https://github.com/hpc-io/drishti-io) is a command-line tool to guide end-users in optimizing
I/O in their applications by detecting typical I/O performance pitfalls and providing a set of
recommendations. Drishti takes as input a Darshan profiling log and optional arguments to report the
various (file-based and overall) performance issues it detects through analysis and heuristics, and
provides actionable tasks for potentially resolving the issues. 

To generate a Drishti report at NERSC you can use a [Shifter
container](../../../development/containers/shifter/index.md):

```shell
shifter --image=docker:hpcio/drishti -- drishti $LOGFILE
```

Make sure to follow the guidelines available at the [Darshan docs](./index.md) (how to compile your
application with Darshan, how to enable it for non-MPI applications, etc), then provide the
`.darshan` file as input to Drishti. Refer to the [Darshan
documentation](./index.md#producing-reports) to find the logs produced by your applications.

Several options are available in Drishti, including a verbose mode that shows sample code snippets
to guide end-users to fix the detected I/O performance issues. 

```shell
usage: drishti [-h] [--issues] [--html] [--svg] [--verbose] [--code] [--path] [--csv] darshan

Drishti:

positional arguments:
  darshan     Input .darshan file

optional arguments:
  -h, --help  show this help message and exit
  --issues    Only displays the detected issues and hides the recommendations
  --html      Export the report as an HTML page
  --svg       Export the report as an SVG image
  --verbose   Display extended details for the recommendations
  --code      Display insights identification code
  --path      Display the full file path for the files that triggered the issue
  --csv       Export a CSV with the code of all issues that were triggered
```

Drishti will display the report in your console:

![advisor result](drishti-sample.svg)
